# StudentManagerSSM

#### 介绍
基于easyUI+SSM的学生管理系统

管理员:管理员信息/年段/班级/学生信息的增删改查
学生：年段/班级信息查询，个人信息修改

#### 安装教程

1.  将SQL导入navicat
2.  idea导入项目，部署Tomcat启动即可

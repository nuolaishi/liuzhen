$(function() {
	//创建画布
	var canvas = document.createElement("canvas");
	document.body.appendChild(canvas);
	canvas.width = $(window).width();
	canvas.height = $(window).height();
	var ctx = canvas.getContext('2d');


	//创建提取文字画布
	var textCanvas = document.createElement("canvas");
	textCanvas.width=1000;
	textCanvas.height=300;
	var textctx = textCanvas.getContext("2d");

	//两个画布透明度不同，所以背景不同，不停刷新2种背景造成明亮相间
	//把第二个画布透明度设置成1


	var particles = [];//字体烟花粒子数组

	var timeNum=false;//时间计算变量
	var nowTime=new Date();
	var lastTime=nowTime.getTime() + 3000;

	// objects
	var listFire = [];
	var listFirework = [];

	var listSpecial = [];
	var listSpark = [];
	var lights = [];
	var fireNumber = 10;
	var center = { x: canvas.width / 2, y: canvas.height / 2 };
	var range = 100;
	var fired = 0;
	var onHold = 0;
	var supprise = false;

	var actions = [makeHeartFirework];

	for (var i = 0; i < fireNumber; i++) {

		var fire = {
			x: Math.random() * range / 2 - range / 4 + center.x,
			y: Math.random() * range * 2.5 + canvas.height,
			size: Math.random() + 0.5,
			fill: '#ff3',
			vx: Math.random() - 0.5,
			vy: -(Math.random() + 4),
			ax: Math.random() * 0.06 - 0.03,
			delay: Math.round(Math.random() * range) + range * 4,
			hold: false,
			alpha: 1,
			far: Math.random() * range + (center.y - range)
		};
		fire.base = {
			x: fire.x,
			y: fire.y,
			vx: fire.vx,
			vy: fire.vy
		};
		//
		listFire.push(fire);
		// play sound
		playLaunchSound();
	}
	// define array of sound
	var listExpSound = $('audio.exp');
	var listLaunchSound = $('audio.launch');

	// define array position of text





	//初始化火星
	function initSpark() {
		var x = Math.random() * range * 3 - range * 1.5 + center.x;
		var vx = Math.random() - 0.5;
		var vy = -(Math.random() + 4);
		var ax = Math.random() * 0.04 - 0.02;
		var far = Math.random() * range * 4 - range + center.y;
		var direct = ax * 10 * Math.PI;
		var max = fireNumber * 0.5;
		for (var i = 0; i < max; i++) {

			var special = {
				x: x,
				y: Math.random() * range * 0.25 + canvas.height,
				size: Math.random() + 2,
				fill: '#ff3',
				vx: vx,
				vy: vy,
				ax: ax,
				direct: direct,
				alpha: 1
			};
			special.far = far - (special.y - canvas.height);
			listSpecial.push(special);
			// play sound
			playLaunchSound();
		}
	}
	//边缘颜色
	function randColor() {
		var r = Math.floor(Math.random() * 256);
		var g = Math.floor(Math.random() * 256);
		var b = Math.floor(Math.random() * 256);
		var color = 'rgb($r, $g, $b)';
		color = color.replace('$r', r);
		color = color.replace('$g', g);
		color = color.replace('$b', b);
		return color;
	}

	function playExpSound() {
		var sound = listExpSound[Math.floor(Math.random() * listExpSound.length)];
		sound.volume = Math.random() * 0.4 + 0.1;
		sound.play();
	}

	function playLaunchSound() {
		setTimeout(function() {
			var sound = listLaunchSound[Math.floor(Math.random() * listLaunchSound.length)];
			sound.volume = 0.05;
			sound.play();
		}, 200);
	}





	//心形烟花
	function makeHeartFirework(fire) {
		var color = randColor();
		var velocity = Math.random() * 3 + 3;
		var max = fireNumber * 5;
		var rotate = Math.random() * Math.PI * 2;
		for (var i = 0; i < max; i++) {
			var rad = (i * Math.PI * 2) / max + rotate;
			var v, p;
			if (rad - rotate < Math.PI * 0.5) {
				p = (rad - rotate) / (Math.PI * 0.5);
				v = velocity + velocity * p;
			}
			else if (rad - rotate > Math.PI * 0.5 && rad - rotate < Math.PI) {
				p = (rad - rotate - Math.PI * 0.5) / (Math.PI * 0.5);
				v = velocity * (2 - p);
			}
			else if (rad - rotate > Math.PI && rad - rotate < Math.PI * 1.5) {
				p = (rad - rotate - Math.PI) / (Math.PI * 0.5);
				v = velocity * (1 - p);
			}
			else if (rad - rotate > Math.PI * 1.5 && rad - rotate < Math.PI * 2) {
				p = (rad - rotate - Math.PI * 1.5) / (Math.PI * 0.5);
				v = velocity * p;
			}
			else {
				v = velocity;
			}
			v = v + (Math.random() - 0.5) * 0.25;
			var firework = {
				x: fire.x,
				y: fire.y,
				size: Math.random() + 1.5,
				fill: color,
				vx: Math.cos(rad) * v,
				vy: Math.sin(rad) * v,
				ay: 0.02,
				alpha: 1,
				life: Math.round(Math.random() * range / 2) + range / 1.5
			};
			firework.base = {
				life: firework.life,
				size: firework.size
			};
			listFirework.push(firework);
		}
		return color;
	}

	//火星
	function makeSpark(special) {
		var color = special.fill;
		var velocity = Math.random() * 6 + 12;
		var max = fireNumber;
		for (var i = 0; i < max; i++) {
			var rad = (Math.random() * Math.PI * 0.3 + Math.PI * 0.35) + Math.PI + special.direct;
			var spark = {
				x: special.x,
				y: special.y,
				size: Math.random() + 1,
				fill: color,
				vx: Math.cos(rad) * velocity + (Math.random() - 0.5) * 0.5,
				vy: Math.sin(rad) * velocity + (Math.random() - 0.5) * 0.5,
				ay: 0.02,
				alpha: 1,
				rad: rad,
				direct: special.direct,
				chain: Math.round(Math.random() * 2) + 2,
				life: Math.round(Math.random() * range / 2) + range / 2
			};
			spark.base = {
				life: spark.life,
				velocity: velocity
			};
			listSpark.push(spark);
		}
		return color;
	}
	//一连串火星
	function chainSpark(parentSpark) {
		var color = parentSpark.fill;
		if (parentSpark.chain > 0) {
			var velocity = parentSpark.base.velocity * 0.6;
			var max = Math.round(Math.random() * 5);
			for (var i = 0; i < max; i++) {

				var rad = (Math.random() * Math.PI * 0.3 - Math.PI * 0.15) + parentSpark.rad + parentSpark.direct;
				var spark = {
					x: parentSpark.x,
					y: parentSpark.y,
					size: parentSpark.size * 0.6,
					fill: color,
					vx: Math.cos(rad) * velocity + (Math.random() - 0.5) * 0.5,
					vy: Math.sin(rad) * velocity + (Math.random() - 0.5) * 0.5,
					ay: 0.02,
					alpha: 1,
					rad: rad,
					direct: parentSpark.direct,
					chain: parentSpark.chain,
					life: parentSpark.base.life * 0.8
				};
				spark.base = {
					life: spark.life,
					size: spark.size,
					velocity: velocity
				};
				listSpark.push(spark);
			}

			if (Math.random() > 0.9 && parentSpark.chain > 1) {
				// play sound
				playExpSound();
			}
		}
		return color;
	}



	function update() {
		// update fire logic 更新火焰逻辑
		for (var i = 0; i < listFire.length; i++) {
			var fire = listFire[i];
			//
			if (fire.y <= fire.far) {
				// play sound
				playExpSound();
				// case add firework
				fired++;
				var color = actions[Math.floor(Math.random() * actions.length)](fire);
				// light
				lights.push({ x: fire.x, y: fire.y, color: color, radius: range * 2 });
				// reset
				fire.y = fire.base.y;
				fire.x = fire.base.x;
				// special
				if (fired % 33 == 0) {
					initSpark();
				}
				// on hold
				supprise = fired % 100 == 0 ? true : supprise;
				if (supprise) {
					fire.vx = 0;
					fire.vy = 0;
					fire.ax = 0;
					fire.hold = true;
					onHold++;
				}
				else {
					fire.vx = fire.base.vx;
					fire.vy = fire.base.vy;
					fire.ax = Math.random() * 0.06 - 0.03;
					// play sound
					playLaunchSound();
				}
			}
			//
			if (fire.hold && fire.delay <= 0) {
				onHold--;
				fire.hold = false;
				fire.delay = Math.round(Math.random() * range) + range * 4;
				fire.vx = fire.base.vx;
				fire.vy = fire.base.vy;
				fire.ax = Math.random() * 0.06 - 0.03;
				fire.alpha = 1;
				// play sound
				playLaunchSound();
			}
			else if (fire.hold && fire.delay > 0) {
				fire.delay--;
			}
			else {
				fire.x += fire.vx;
				fire.y += fire.vy;
				fire.vx += fire.ax;
				fire.alpha = (fire.y - fire.far) / fire.far;
			}
		}

		// update firework logic
		for (var i = listFirework.length - 1; i >= 0; i--) {
			var firework = listFirework[i];

			if (firework) {
				firework.vx *= 0.9;
				firework.vy *= 0.9;
				firework.x += firework.vx;
				firework.y += firework.vy;
				firework.vy += firework.ay;
				firework.alpha = firework.life / firework.base.life;
				firework.size = firework.alpha * firework.base.size;
				firework.alpha = firework.alpha > 0.6 ? 1 : firework.alpha;
				//
				firework.life--;
				if (firework.life <= 0) {
					listFirework.splice(i, 1);
				}
			}
		}

		// supprise happy new year!
		if (supprise && onHold == 10) {
			//这里只进来一次

			supprise = false;
			//这里执行最后的庆祝字体
			//	var tamp = sleep(3000);qwy
			if(timeNum){
				//什么也不做
			}else{
				nowTime = new Date();//当前时间
				timeNum=true;
				lastTime = nowTime.getTime() + 3000;//3秒后
			}
		}
		// console.log("");
		if(timeNum){

			if (new Date().getTime() > lastTime) {
				//	setTimeout(createFireworks(canvas.width/2,canvas.height/6),1000);
				createFireworks(canvas.width/2,canvas.height/6);//床罩文字粒子
				timeNum=false;
			}
		}





		// update text logic 更新文本逻辑  console.log("检查");


		// update special logic
		for (var i = listSpecial.length - 1; i >= 0; i--) {

			var special = listSpecial[i];
			if (special.y <= special.far) {
				// play sound
				playExpSound();
				// light
				lights.push({ x: special.x, y: special.y, color: special.fill, alpha: 0.02, radius: range * 2 });
				//
				makeSpark(special);
				// remove from list
				listSpecial.splice(i, 1);
			}
			else {
				special.x += special.vx;
				special.y += special.vy;
				special.vx += special.ax;
				special.alpha = (special.y - special.far) / special.far;
			}
		}

		// update spark logic
		for (var i = listSpark.length - 1; i >= 0; i--) {

			var spark = listSpark[i];
			if (spark) {
				spark.vx *= 0.9;
				spark.vy *= 0.9;
				spark.x += spark.vx;
				spark.y += spark.vy;
				spark.vy += spark.ay;
				spark.alpha = spark.life / spark.base.life + 0.2;
				//
				spark.life--;
				if (spark.life < spark.base.life * 0.8 && spark.life > 0) {
					//
					spark.chain--;
					chainSpark(spark);
				}
				if (spark.life <= 0) {
					listSpark.splice(i, 1);
				}
			}
		}
	}

	function draw() {

		// clear
		ctx.globalCompositeOperation = 'source-over';
		ctx.globalAlpha = 0.2;
		ctx.fillStyle = '#000000';
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		// re-draw
		ctx.globalCompositeOperation = 'screen';

		for (var i = 0; i < listFire.length; i++) {

			var fire = listFire[i];
			ctx.globalAlpha = fire.alpha;
			ctx.beginPath();
			ctx.arc(fire.x, fire.y, fire.size, 0, Math.PI * 2);
			ctx.closePath();
			ctx.fillStyle = fire.fill;
			ctx.fill();
		}

		for (var i = 0; i < listFirework.length; i++) {

			var firework = listFirework[i];
			ctx.globalAlpha = firework.alpha;
			ctx.beginPath();
			ctx.arc(firework.x, firework.y, firework.size, 0, Math.PI * 2);
			ctx.closePath();
			ctx.fillStyle = firework.fill;
			ctx.fill();
		}

		for (var i = 0; i < listSpecial.length; i++) {

			var special = listSpecial[i];
			ctx.globalAlpha = special.alpha;

			ctx.fillStyle = special.fill;
			ctx.fillRect(special.x - special.size, special.y - special.size, special.size * 2, special.size *2);
		}

		for (var i = 0; i < listSpark.length; i++) {

			var spark = listSpark[i];
			ctx.globalAlpha = spark.alpha;

			ctx.fillStyle = spark.fill;
			ctx.fillRect(spark.x - spark.size, spark.y - spark.size, spark.size * 2, spark.size *2);
		}

		// light effect
		while (lights.length) {

			var light = lights.pop();
			var gradient = ctx.createRadialGradient(light.x, light.y, 0, light.x, light.y, light.radius);
			gradient.addColorStop(0, '#fff');
			gradient.addColorStop(0.2, light.color);
			gradient.addColorStop(0.8, 'rgba(0, 0, 0, 0)');
			gradient.addColorStop(1, 'rgba(0, 0, 0, 0)');
			ctx.globalAlpha = light.alpha ? light.alpha : 0.25;
			ctx.fillStyle = gradient;
			ctx.fillRect(light.x - light.radius, light.y - light.radius, light.radius * 2, light.radius * 2);
		}





	}




	(function aaa() {

		update();
		draw();
		drawFireworks();
		requestAnimationFrame(aaa);

	})();







	function createFireworks(x, y) {



		textctx.fillStyle = "#000000";
		textctx.fillRect(0,0,textCanvas.width,textCanvas.height);

		var text = "真真我😙爱你";
		var hue = Math.random() * 360;
		var hueVariance = 30;
		function setupColors(p){
			p.hue = Math.floor(Math.random() * ((hue + hueVariance) - (hue - hueVariance))) + (hue - hueVariance);
			p.brightness = Math.floor(Math.random() * 21) + 50;
			p.alpha = (Math.floor(Math.random() * 61) + 40) / 100;
		}

		var gap = 6;//获取字体间隔，决定字体的像素多少

		var fontSize = 120;
		textctx.font=fontSize+"px Verdana";
		textctx.fillStyle = "#ffffff";
		var textWidth = textctx.measureText(text).width;
		var textHeight = fontSize;
		textctx.fillText(text,0,textHeight);
		var imgData = textctx.getImageData(0,0,textWidth,textHeight*1.2);
		textctx.fillStyle = "#000000";
		textctx.fillRect(0,0,textCanvas.width,textCanvas.height);
		for (var h = 0; h < textHeight*1.2; h+=gap) {
			for(var w = 0; w < textWidth; w+=gap){
				var position = (textWidth * h + w) * 4;
				var r = imgData.data[position], g = imgData.data[position + 1], b = imgData.data[position + 2], a = imgData.data[position + 3];
				if(r+g+b==0)continue;
				var p = {};
				p.x = x;
				p.y = y;
				p.fx = x + w - textWidth/2;
				p.fy = y + h - textHeight/2;
				p.size = Math.floor(Math.random()*2)+1;
				p.speed = 1;
				setupColors(p);
				particles.push(p);
			}
		}
		var sound = listExpSound[22];
		sound.volume = 1;//将音量设置为1
		sound.play();
	}

	function drawFireworks() {


		ctx.globalAlpha = 1;
		ctx.globalCompositeOperation = 'destination-out';
		ctx.fillStyle ='rgba(0,0,0,'+10/100+')';
		ctx.fillRect(0,0,canvas.width,canvas.height);
		ctx.globalCompositeOperation = 'lighter';

		ctx.fillStyle = "#000000";
		ctx.fillRect(0, 0, canvas.width, canvas.height);

		for (var i = 0; i < particles.length; i++) {
			var p = particles[i];
			p.x += (p.fx - p.x)/10;
			p.y += (p.fy - p.y)/10-(p.alpha-1)*p.speed;
			p.alpha -= 0.006;
			//	p.alpha -= 0.001;
			if (p.alpha<=0) {
				particles.splice(i, 1);
				continue;
			}
			ctx.beginPath();
			ctx.arc(p.x, p.y, p.size, 0, Math.PI * 2, false);
			ctx.closePath();
			ctx.fillStyle = 'hsla('+p.hue+',100%,'+p.brightness+'%,'+p.alpha+')';
			ctx.fill();
		}
	}
/*
	document.addEventListener("mousedown", mouseDownHandler);//添加鼠标监听时间
	function mouseDownHandler(e) {
		var x = e.clientX;
		var y = e.clientY;
		createFireworks(x, y);
	}
	*/
})